﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;

namespace Miacid_Lotus.Source_Files
{
    class Transition
    {
	    public static baseState doTransition(baseState currentState, Player player){
            if (curState.valid(player))
                return curState;

            else
            {
                while (!curState.valid(player))
                    curState = curState.next();
            }

            return curState;
        }
        public Transition() { }
        public ~Transition() { }

    }
}
