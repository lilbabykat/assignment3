﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;

namespace Miacid_Lotus.Source_Files
{
    class moveTwoStack
    {
        public moveTwoStack() { }
        public ~moveTwoStack() { }
        public virtual bool valid(Player player) {
            int tempPos = 0;
            for (int i = 0; i < 8; ++i)
            {
                if (GameData().board.IsPieceOnTop(player.piece, i))
                {
                    tempPos = GameData().board.GetSizeOfStack(GameData().board.GetSizeOfStack(i) + i);
                    if (tempPos >= 2
                        && tempPos + (GameData().board.GetSizeOfStack(tempPos) == 10))
                    {
                        bPos = i;
                        ePos = tempPos;
                        return isValid = true;
                    }
                }
            }
            return isValid = false;
        
        }
        public virtual baseState next() {
            return new moveByOne();
        }

    }
}
