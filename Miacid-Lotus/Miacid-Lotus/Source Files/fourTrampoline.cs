﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miacid_Lotus.Source_Files
{
    class fourTrampoline
    {
        public fourTrampoline() {
            isValid = false;
        }
	    public ~fourTrampoline(){}
        public virtual bool valid(Player player) {
            if ((GameData().board.GetSizeOfStack(0) == 7) && GameData().board.IsPieceOnTop(player.piece, 0))
            {	//if beginning on the left track
                bPos = 0;
                ePos = 10;
                return isValid = true;
            }
            if (GameData().board.GetSizeOfStack(3) == 7 && GameData().board.IsPieceOnTop(player.piece, 3))
            {	//if beginning on the right track
                bPos = 3;
                ePos = 10;
                return isValid = true;
            }
            if (GameData().board.GetSizeOfStack(1) == 6 && GameData().board.IsPieceOnTop(player.piece, 1))
            {
                bPos = 1;
                ePos = 10;
                return isValid = true;
            }
            if (GameData().board.GetSizeOfStack(4) == 6 && GameData().board.IsPieceOnTop(player.piece, 4))
            {
                bPos = 4;
                ePos = 10;
                return isValid = true;
            }
            if (GameData().board.GetSizeOfStack(2) == 5 && GameData().board.IsPieceOnTop(player.piece, 2))
            {
                bPos = 2;
                ePos = 10;
                return isValid = true;
            }
            if (GameData().board.GetSizeOfStack(5) == 5 && GameData().board.IsPieceOnTop(player.piece, 5))
            {
                bPos = 5;
                ePos = 10;
                return isValid = true;
            }
            if (GameData().board.GetSizeOfStack(6) == 4 && GameData().board.IsPieceOnTop(player.piece, 6))
            {
                bPos = 6;
                ePos = 10;
                return isValid = true;
            }
            return isValid = false;
        }
        public virtual baseState next() {
            return new moveTwoStack();
        }
    }
}
