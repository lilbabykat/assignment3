﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miacid_Lotus.Source_Files
{
    class exitState
    {
        public exitState(){
            isValid = false;
        }
        public ~exitState(){}
		public virtual bool valid(Player player){
            for (int i = 0; i <= MAX_GAME_POSITIONS; i++)
            {
                if (GameData().board.GetSizeOfStack(i) + i >= MAX_GAME_POSITIONS && GameData().board.IsPieceOnTop(player.piece, i))
                {
                    bPos = i;
                    ePos = i + GameData().board.GetSizeOfStack(i);
                    return isValid = true;
                }
            }
            return isValid = false;
        }
		public virtual baseState next(){
            return new fourTrampoline();
        }

    }
}
