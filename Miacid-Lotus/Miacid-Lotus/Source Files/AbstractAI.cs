﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;

namespace Miacid_Lotus.SourceFile
{
    abstract class AbstractAI
    {
        public virtual void PerformTurn(Player p);
        public virtual ~AbstractAI();

    }
}
