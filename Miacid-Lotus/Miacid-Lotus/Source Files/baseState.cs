﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miacid_Lotus.Source_File
{
    class baseState
    {
        public baseState() { }
        public ~baseState(){
            isValid = false;
            bPos = 0;
            ePos = 0;
        }

        public virtual bool valid(Player player){}
	    public virtual baseState next(){
            isValid = false;
            bPos = 0;
            ePos = 0;
        }
    	public virtual int getBPos(){return bPos;}
    	public virtual int getEPos(){return ePos;}

        protected bool isValid;
        protected int bPos;
        protected int ePos;
    }
}
