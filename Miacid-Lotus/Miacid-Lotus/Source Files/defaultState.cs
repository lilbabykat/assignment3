﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;


namespace Miacid_Lotus.SourceFile
{
    class defaultState
    {
        public defaultState (){
            isValid = false;
        }
        ~defaultState() { }
        virtual bool valid(Player player) {
            for (int i = 0; i < MAX_GAME_POSITIONS; ++i)
            {
                if (GameData().board.IsPieceOnTop(player.piece, i))
                    return isValid = false;
            }
            for (int i = -12; i < 0; ++i)
                if (GameData().board.IsPieceOnTop(player.piece, i))
                {
                    bPos = i;
                    ePos = GameData().board.GetSizeOfStack(i);
                    return isValid = true;
                }
            return isValid = false;
        }
        virtual baseState next() {
            return new exitState();
        }

    }
}
