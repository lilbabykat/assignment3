﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;

namespace Miacid_Lotus.Source_Files
{
    class highStackState
    {
	    public highStackState(){}
	    public ~highStackState(){}
	    public virtual baseState next(){
            return new moveByOne();
        }
	    public virtual bool valid(Player player){
            int highestTargetStack = -1;
	        int tempPos = 0;
	        for(int i = 0; i < MAX_GAME_POSITIONS-tempPos; ++i){
		        if(GameData().board.IsPieceOnTop(player.piece, i))
			        tempPos = GameData().board.GetSizeOfStack(i);
		        else continue;
		        if(GameData().board.GetSizeOfStack(tempPos+i) > 2){
			        if(GameData().board.GetSizeOfStack(tempPos+i) > GameData().board.GetSizeOfStack(highestTargetStack))
				        highestTargetStack = tempPos+i;
		        }
	        }

	        if(highestTargetStack > 0){
		        bPos = tempPos;
		        ePos = highestTargetStack;
		        return isValid = true;
	        }
	        return isValid = false;}

}
