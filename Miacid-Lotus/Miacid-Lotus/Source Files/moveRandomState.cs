﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;

namespace Miacid_Lotus.SourceFile
{
    class moveRandomState
    {
        public virtual bool valid(Player p){}
        public virtual baseState next() {return new exitState() ;}
        public static bool wasCalled = false;

        moveRandomState() { }
        virtual ~moveRandomState() { }

        bool valid(Player player) { 
        if(wasCalled){
            wasCalled = false;
            return isValid = false;
        }
        for (int i = 0; i < MAX_GAME_POSITIONS; ++i)
        {
            if (GameData().board.IsPieceOnTop(player.piece, i))
            {
                bPos = i;
                ePos = GameData().board.GetSizeOfStack(i) + i;
                if (ePos > 2 && bPos <= 2)
                    ePos += 3;
                wasCalled = true;
                return isValid = true;
            }
        }
    }
}