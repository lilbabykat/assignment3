﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miacid_Lotus.Source_Files
{
    class moveByOne
    {
        public moveByOne() {
            isValid = false;
        }
        public ~moveByOne() {
            isValid = false;
        }
        public virtual bool valid(Player player) {
            int movablePieces = 0;
            int canMoveByOne = 0;
            for (int i = 0; i < MAX_GAME_POSITIONS; ++i)
            {
                if (GameData().board.IsPieceOnTop(player.piece, i))
                {
                    ++movablePieces;
                    if (GameData().board.GetSizeOfStack(i) == 1)
                        ++canMoveByOne;
                }
            }
            if (movablePieces == canMoveByOne)
            {
                for (int i = 0; i < MAX_GAME_POSITIONS; ++i)
                {
                    if (GameData().board.IsPieceOnTop(player.piece, i))
                    {
                        bPos = i;
                        ePos = i + 1;
                        return isValid = true;
                    }
                }
            }
            return isValid = false;
        }
        public virtual baseState next() {}

    }
}
