﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;




namespace Miacid_Lotus.Board
{
    class Board
    {
        public const int MAX_START_STACKS       = 12; // maximum amount of stacks for players at beginning of game
        public const int MAX_GAME_POSITIONS     = 17; // 17 spots on the board
        public const int PCS_PER_PLAYER_2       = 10;
        public const int PCS_PER_PLAYER_3OR4    = 6;
        public const int LOTUS_TRAMPOLINE       = 10; //the id is 10, the actual pice is 11 by the document given


        private int numplayers;
        private Stack<PIECE>[] start =  new Stack<PIECE>[12];
        private Stack<PIECE>[] position = new Stack<PIECE>[17];
        private Vector<PIECE> finish;

        private bool movingpiece; // if we are in the state of moving a piece
        private int  movingstart; // start here...
        private int  movingend;   // go to here

        public int numstartstacks;
        
        Board() {

        }
    }
}
