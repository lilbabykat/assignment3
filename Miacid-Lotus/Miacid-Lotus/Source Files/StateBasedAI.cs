﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Miacid_Lotus.Data;

namespace Miacid_Lotus.Source_Files
{
    class StateBasedAI
    {
		public StateBasedAI(){
            currentState = new defaultState();
        }
		public ~StateBasedAI(){
            delete currentState;
        }
		public virtual void PerformTurn(Player player){
            currentState = Transition::doTransition(currentState, player);
	        GameData().board.MovePiece(currentState.getBPos(), currentState->getEPos());
        }
        private baseState currentState { }

    }
}
