﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miacid_Lotus.Data
{
    enum TMove { TM_NONE, TM_START, TM_FORWARD, TM_ATTACK };
    struct TRule
    {

        public TMove thirdLast;
        public TMove secondLast;
        public TMove last;
        public TMove result;
        public bool matched;
        public int weight;
        public int rulenum;

        TRule()
        {
            this.thirdLast = TMove.TM_NONE;
            this.secondLast = TMove.TM_NONE;
            this.last = TMove.TM_NONE;
            this.result = TMove.TM_NONE;
            this.matched = false;
            this.weight = 0;
            this.rulenum = 0;
        }

        void SetRule(int num, TMove conditionA, TMove conditionB, TMove conditionC, TMove resultD)
        {
            this.thirdLast = conditionA;
            this.secondLast = conditionB;
            this.last = conditionC;
            this.result = resultD;
            this.matched = false;
            this.weight = 0;
            this.rulenum = 0;
        }
    }
}